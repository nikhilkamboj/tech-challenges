package com.challenge.techgig.cs_internal;

import java.util.Scanner;

public class Input {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testCases = scanner.nextInt();
        int arrayLength;

        for (int i = 0; i < testCases; i++) {
            arrayLength = scanner.nextInt();
            System.out.println("array length: " + arrayLength);
            int[] array = new int[arrayLength];
            for (int j = 0; j < arrayLength; j++) {
                array[i] = scanner.nextInt();
                System.out.print(array[i] + " ");
            }
            System.out.println();
        }
    }
}
