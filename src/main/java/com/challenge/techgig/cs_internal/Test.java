package com.challenge.techgig.cs_internal;

public class Test {

    public static void main(String[] args) {
        String s1 = "35";
        String s2 = "44";
        int i = getMax1stElement(s1,s2);
        if (i == 1) {
            System.out.println(s1);
        } else{
            System.out.println(s2);
        }
    }

    private static int getMax1stElement(String s1, String s2) {
        int i1 = Integer.valueOf(s1.charAt(0));
        int i2 = Integer.valueOf(s2.charAt(0));
        return i1 >= i2? 1:2;
    }
}
