package com.challenge.techgig.semi_final;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class MaxSubSet {
    private static int currentElement;
    private static int currentIterationSum;
    private static ArrayList<Integer> eachIterationSumList = new ArrayList<Integer>();
    private static int max;
    public static void main(String[] args) {

//        Scanner scanner = new Scanner(System.in);
//        int numberOfTestCases = scanner.nextInt();
//        for (int i = 0; i < numberOfTestCases; i++) {
//            int arrayLength = scanner.nextInt();
//            int[] array = new int[arrayLength];
//            for (int j = 0; j < arrayLength; j++) {
//                array[j] = scanner.nextInt();
//            }
//            max = 0;
            int[] array = {36,8,16,48,35,47};
            getSum(array);

    }


    private static void getSum(int[] testArray) {
        for (int i = 0; i < testArray.length; i++) {
            currentElement = testArray[i];
            currentIterationSum = testArray[i];
            HashMap<Integer,Integer> hashMap = new HashMap<Integer, Integer>();
            initializeHashMap(hashMap);
            addCurrentToMap(currentElement,hashMap);
            for (int j = 0; j < testArray.length; j++) {
                getDigitsFromNumber(testArray[j],hashMap);
            }
            if (currentIterationSum > max) {
                max = currentIterationSum;
            }
            System.out.println("\n" + currentIterationSum);
            eachIterationSumList.add(currentIterationSum);
            currentIterationSum = 0;
        }
        System.out.println(max);
    }

    private static void getDigitsFromNumber(int number, HashMap<Integer,Integer> map) {
        boolean isDigitPresentInMap = false;
        ArrayList<Integer> digitList = new ArrayList<Integer>();
        int temp = number;
        int digit;
        while (temp > 0) {
            digit = temp%10;
            if (map.get(digit) == -1) {
                isDigitPresentInMap = true;
                digitList.add(digit);
            } else if (map.get(digit) == currentElement) {
                isDigitPresentInMap = false;
                return;
            } else if (map.get(digit) < number) {
                isDigitPresentInMap = true;
                digitList.add(digit);
            } else if (map.get(digit) > number) {
                return;
            }
            temp = temp/10;
        }
        if (isDigitPresentInMap) {
            int toBeReplaced = 0;
            for (int i: digitList
                    ) {
                int replaced = map.remove(i);
                if (replaced != number && replaced > toBeReplaced) {
                    toBeReplaced = replaced;
                }
                map.put(i,number);
            }
            if (toBeReplaced == 0) {
                currentIterationSum = currentIterationSum + number;
            } else {
                currentIterationSum = currentIterationSum - toBeReplaced + number;
            }
            System.out.print(" " + currentIterationSum);
        }
    }

    private static void initializeHashMap(HashMap<Integer,Integer> hashMap) {
        for (int i = 0; i <= 9; i++) {
            hashMap.put(i,-1);
        }
    }

    private static void addCurrentToMap(int currentNumber, HashMap<Integer,Integer> map) {
        int temp1 = currentNumber;
        int temp2 = 0;
        while (temp1 > 0) {
            temp2 = temp1%10;
            map.remove(temp2);
            map.put(temp2,currentNumber);
            temp1 = temp1/10;
        }
    }
}
