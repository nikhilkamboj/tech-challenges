package com.challenge.techgig.cs_internal;

import java.util.ArrayList;

public class MakeList {

    private static ArrayList<Integer> list = new ArrayList<Integer>();

    public static void main(String[] args) {
        Node root = new Node();
        Node root1 = new Node();
        int[] array = {5,10 ,4 ,-1,6 };

        root = createCleanTree(array, 0, root);
        root1 = createCleanTree(array,1,root1);
        System.out.print("tree 1 is :");
        print(root);
        System.out.println();
        System.out.print("tree 2 is :");
        print(root1);
//        // 1. store sums in two variables and then which one is greater print that string.
        int sum1 = getMaximumSumPath(root);
        int sum2 = getMaximumSumPath(root1);
        String mainString = "";
        String tree1String = getMaximumSumPathPattern(root);
        String tree2String = getMaximumSumPathPattern(root1);
//
        if (sum1 == sum2) {
            // change logic to 1. get 1st element of the string and then convert to int and check
            // tree having larger root is the answer
            // method telling which string has larger 1st element
            if (getMax1stElement(tree1String,tree2String) == 1) {
                mainString = tree1String;
            } else {
                mainString = tree2String;
            }
        }

//        mainString = sum1>sum2?tree1String:tree2String;
//        System.out.println(mainString);
//        System.out.println("\n" + "for root 1 max sum :" + sum1);
//        System.out.println("for root 2 max sum:" + sum2);
//        System.out.println("for root 1 pattern is : " + tree1String);
//        System.out.println("for root 2 pattern is : " + tree2String);
    }

    private static int getMax1stElement(String s1, String s2) {
        int size1 = s1.length();
        int i1 = Integer.valueOf(s1.charAt(size1-1));
        int size2 = s2.length();
        int i2 = Integer.valueOf(s2.charAt(size2-1));
        return i1 >= i2? 1:2;
    }

    // re write create tree logic all together
    private static Node createTree(int[] array, int index, Node root) {
        if (index >= array.length) {
            return root;
        }

        // check if index+2 is negative.
        // if yes get 1st non negative non zero value
        // for 2nd path just get the next positive value after 1st path
        int firstIndex = 0;
        int secondIndex = 0;
        if (array[index+2] <= 0) {
            firstIndex = getPositiveNonZeroIndex(array,index);
            if (firstIndex == -1) {
                return root;
            }
            if (array[firstIndex+1] > 0) {
                secondIndex = firstIndex+1;
            } else {
                secondIndex = getPositiveNonZeroIndex(array,firstIndex+1);
            }
            root = new Node();
            root.data = array[index];
            root.left = createTree(array, firstIndex, root.left);
            root.right = createTree(array, secondIndex, root.right);
        } else {
            root = new Node();
            root.data = array[index];
            root.left = createTree(array, index + 2, root.left);
            root.right = createTree(array, index + 3, root.right);
        }

        return root;
    }

    private static Node createCleanTree(int[] array, int index, Node root) {
        if (index >= array.length) {
            return root;
        }


        root = new Node();
        root.data = array[index];

        if (index+2 >= array.length) {
            return root;
        }

        if (array[index+2] <= 0) {
            int indexPath1 = getPositiveNonZeroIndex(array,index);
            int indexPath2 = firstPositiveNonZeroIndex(array,indexPath1+1);
            root.left = createCleanTree(array, indexPath1, root.left);
            root.right = createCleanTree(array, indexPath2, root.right);
        } else {
            root.left = createCleanTree(array, index + 2, root.left);
            root.right = createCleanTree(array, index + 3, root.right);
        }

        return root;
    }

    private static void print(Node root) {
        if (root == null) {
            return;
        }
        print(root.left);
        System.out.print(root.data + " ");
        print(root.right);
    }

    private static int getMaximumSumPath(Node root) {
        if (root == null) {
            return 0;
        }
        if (root.left == null && root.right == null) {
            return root.data;
        }
        int leftSubTreeHeight = getMaximumSumPath(root.left);
        int rightSubTreeHeight = getMaximumSumPath(root.right);
        return Math.max(leftSubTreeHeight, rightSubTreeHeight) + root.data;
    }

    private static String getMaximumSumPathPattern(Node root) {
        if (root == null) {
            return "";
        }
        if (root.left == null && root.right == null) {
            return String.valueOf(root.data);
        }
        String leftSubTreeHeight = getMaximumSumPathPattern(root.left);
        String rightSubTreeHeight = getMaximumSumPathPattern(root.right);
        Integer leftVal = leftSubTreeHeight == ""?0:Integer.valueOf(leftSubTreeHeight);
        Integer rightVal = rightSubTreeHeight == ""?0:Integer.valueOf(rightSubTreeHeight);
        int i = leftVal.compareTo(rightVal);
        String greater = i>0?leftSubTreeHeight:rightSubTreeHeight;
        return greater + root.data;
    }

    private static int[] getPositiveArray(int[] array) {
        for (int i = 0;i < array.length;i++) {
            if (array[i] > 0) {
                list.add(array[i]);
            }
        }
        int[] newArray = new int[list.size()];
        int j = 0;
        for (int i: list
                ) {
            newArray[j] = i;
            j++;
        }
        return newArray;
    }

    private static int getPositiveNonZeroIndex(int[] array, int index) {
        return firstPositiveNonZeroIndex(array,index);
    }

    private static int firstPositiveNonZeroIndex(int[] array, int index) {
        while (index < array.length) {
            if (array[index] <= 0) {
                index++;
            } else {
                return index;
            }
        }
        // all ahead are negative or zero
        return -1;
    }
}
