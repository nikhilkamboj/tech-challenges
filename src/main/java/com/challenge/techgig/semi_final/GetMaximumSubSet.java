package com.challenge.techgig.semi_final;

import javax.naming.InsufficientResourcesException;
import java.util.ArrayList;
import java.util.HashMap;

public class GetMaximumSubSet {

    // 14,12,23,45,39
    // long as data type
    // maintain hashmap for each iteration to keep track of digits occurred till now
    // add element if digits are unique for the iteration but what if digits came before,
    // then check for the greatest till arrived for that digit
    static Integer currentElement;
    public static void main(String[] args) {
        int[] array = {14,12,23,33,45,39};
        int maxSum = 0;
        for (int i = 0; i < array.length; i++) {
            HashMap<Integer,Integer> hashMap = new HashMap<Integer, Integer>();
            initializeHaspMap(hashMap);
            currentElement = array[i];
            addCurrentToMap(currentElement,hashMap);
            maxSum = 0;
            for (int j = 0; j < array.length; i++) {
                maxSum += splitAndAddNumber(array[j], hashMap);
                System.out.print(maxSum + " ");
            }

        }


    }

    private static void initializeHaspMap(HashMap<Integer,Integer> hashMap) {
        for (int i = 0; i <= 9; i++) {
            hashMap.put(i,-1);
        }
    }

    private static int splitAndAddNumber(int number, HashMap<Integer,Integer> map) {
        int maxSum = currentElement;
        boolean isDigitPresent = false;
        Integer temp1 = number;
        Integer temp2 = 0;
        ArrayList<Integer> digitsArray = new ArrayList<Integer>();
        while (temp1 > 0) {
            temp2 = temp1%10;
            if (map.get(temp2) == -1) {
                isDigitPresent = false;
            }
            if (map.get(temp2) != -1) {
                if (map.get(temp2) == currentElement) {
                    // no replacement required
                } else if (map.get(temp2) < number) {
                    // replace it hence isPresent flag becomes true
                    isDigitPresent = true;
                } else {
                    // isPresent remains false
                }
            }
            digitsArray.add(temp2);
            temp1 = temp1/10;
        }

        if (isDigitPresent) {
            for (int i: digitsArray
                 ) {
                if (map.get(i) != -1) {
                    int removedNumber = map.remove(i);
                    maxSum -= removedNumber;
                    map.put(i,number);
                } else {
                    map.put(i,number);
                }
                maxSum += number;
            }
        }
        return maxSum;
    }

    private static void addCurrentToMap(int currentNumber, HashMap<Integer,Integer> map) {
        int temp1 = currentNumber;
        int temp2 = 0;
        while (temp1/10 > 0) {
            temp2 = temp1%10;
            map.put(temp2,currentNumber);
            temp1 = temp1/10;
        }
    }

}
