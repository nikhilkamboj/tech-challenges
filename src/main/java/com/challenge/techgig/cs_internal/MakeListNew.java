package com.challenge.techgig.cs_internal;

public class MakeListNew {
    static int greatest=-9999;
    public static void main(String[] args) {
        Node root = new Node();
        Node root1 = new Node();
        int[] array = {-1,-3,-10,-11};

        // check if all negatives then return largest negative
        if (isArrayNegative(array)) {
            System.out.println(greatest);
            return;
        }
        // check for first and second positive values
        if (array.length == 2) {
            System.out.println(array[0]>=array[1]?array[0]:array[1]);
            return;
        }

        // if we have a single or two length array

        //
        int index1 = firstPositiveNonZeroIndex(array,0);
        int index2 = firstPositiveNonZeroIndex(array,index1+1);

        root = createCleanTree(array, index1, root);
        root1 = createCleanTree(array,index2,root1);
        System.out.print("tree 1 is :");
        print(root);
        System.out.println();
        System.out.print("tree 2 is :");
        print(root1);
    }

    private static Node createCleanTree(int[] array, int index, Node root) {
        if (index >= array.length ) {
            return null;
        }


        root = new Node();
        root.data = array[index];

        // check if i+2< and i+3 is greater then move ahead else return
        if (index+2 >= array.length || index+3 >= array.length ) {
            if (index+2 < array.length && array[index+2]>0) {
                root.left = createCleanTree(array, index + 2, root.left);
                root.right = null;
            }
            return root;
        }



        if (array[index+2] <= 0 || array[index+3] <= 0) {
            int indexPath1 = getPositiveNonZeroIndex(array,index+2);
            int indexPath2 = indexPath1==-1?-1:getPositiveNonZeroIndex(array,indexPath1+1);

            root.left = indexPath1 == -1?null:createCleanTree(array, indexPath1, root.left);
            root.right = indexPath2 == -1?null:createCleanTree(array, indexPath2, root.right);
        } else {
            root.left = createCleanTree(array, index + 2, root.left);
            root.right = createCleanTree(array, index + 3, root.right);
        }

        return root;
    }

    private static int getPositiveNonZeroIndex(int[] array, int index) {
        return firstPositiveNonZeroIndex(array,index);
    }

    private static int firstPositiveNonZeroIndex(int[] array, int index) {
        while (index < array.length) {
            if (array[index] <= 0) {
                index++;
            } else {
                return index;
            }
        }
        // all ahead are negative or zero
        return -1;
    }

    private static void print(Node root) {
        if (root == null) {
            return;
        }
        print(root.left);
        System.out.print(root.data + " ");
        print(root.right);
    }

    private static boolean isArrayNegative(int[] array) {
        for (int i: array) {
            if (i > 0) {
                return false;
            }
            if (i > greatest) {
                greatest = i;
            }
        }
        return true;
    }
}
