package com.challenge.techgig.cs_internal;

import java.util.Scanner;

public class TestScanner {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOfTestCases = scanner.nextInt();
        for (int i = 0; i < numberOfTestCases; i++) {
            int arrayLength = scanner.nextInt();
            int[] array = new int[arrayLength];
            for (int j = 0; j < arrayLength; j++) {
                array[i] = scanner.nextInt();
            }
        }
    }
}
