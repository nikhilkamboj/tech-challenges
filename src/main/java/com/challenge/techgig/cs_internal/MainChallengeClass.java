package com.challenge.techgig.cs_internal;

import java.util.Scanner;

public class MainChallengeClass {
    static int greatest=-9999;
    public static void main(String args[] ) throws Exception {

        //Write code here
        Node root = new Node();
        Node root1 = new Node();
        Scanner scanner = new Scanner(System.in);
        int testCases = scanner.nextInt();
        int arrayLength;

        for (int i = 0; i < testCases; i++) {
            arrayLength = scanner.nextInt();
            int[] array = new int[arrayLength];
            for (int j = 0; j < arrayLength; j++) {
                array[j] = scanner.nextInt();
            }

            // check if all negatives then return largest negative
            if (isArrayNegative(array)) {
                System.out.println(greatest);
            } else if (array.length == 2) {
                System.out.println(array[0] >= array[1] ? array[0] : array[1]);
            } else {
                int index1 = firstPositiveNonZeroIndex(array, 0);
                int index2 = firstPositiveNonZeroIndex(array, index1 + 1);

                if (index1 == array.length-1) {
                    System.out.println(array[index1]);
                }else if (index2 == array.length-1) {
                    System.out.println(array[0] >= array[1] ? array[0] : array[1]);
                } else {
                    root = createCleanTree(array, index1, root);
                    root1 = createCleanTree(array, index2, root1);

                    int sum1 = getMaximumSumPath(root);
                    int sum2 = getMaximumSumPath(root1);
                    String mainString = "";
                    String tree1String = getMaximumSumPathPattern(root);
                    String tree2String = getMaximumSumPathPattern(root1);

                    if (sum1 == sum2) {
                        // string having larger 1st element

                        if (getMax1stElement(tree1String, tree2String) == 1) {
                            mainString = tree1String;
                        } else {
                            mainString = tree2String;
                        }
                    }
                    mainString = sum1 > sum2 ? tree1String : tree2String;
                    System.out.println(mainString);
                }
            }
        }

    }

    private static int getMax1stElement(String s1, String s2) {
        int i1 = Integer.valueOf(s1.charAt(0));
        int i2 = Integer.valueOf(s2.charAt(0));
        return i1 >= i2? 1:2;
    }

    private static Node createCleanTree(int[] array, int index, Node root) {
        if (index >= array.length ) {
            return null;
        }


        root = new Node();
        root.data = array[index];

        // check if i+2< and i+3 is greater then move ahead else return
        if (index+2 >= array.length || index+3 >= array.length ) {
            if (index+2 < array.length && array[index+2]>0) {
                root.left = createCleanTree(array, index + 2, root.left);
                root.right = null;
            }
            return root;
        }



        if (array[index+2] <= 0 || array[index+3] <= 0) {
            int indexPath1 = getPositiveNonZeroIndex(array,index+2);
            int indexPath2 = indexPath1==-1?-1:getPositiveNonZeroIndex(array,indexPath1+1);

            root.left = indexPath1 == -1?null:createCleanTree(array, indexPath1, root.left);
            root.right = indexPath2 == -1?null:createCleanTree(array, indexPath2, root.right);
        } else {
            root.left = createCleanTree(array, index + 2, root.left);
            root.right = createCleanTree(array, index + 3, root.right);
        }

        return root;
    }


    private static int getMaximumSumPath(Node root) {
        if (root == null) {
            return 0;
        }
        if (root.left == null && root.right == null) {
            return root.data;
        }
        int leftSubTreeHeight = getMaximumSumPath(root.left);
        int rightSubTreeHeight = getMaximumSumPath(root.right);
        return Math.max(leftSubTreeHeight, rightSubTreeHeight) + root.data;
    }

    private static String getMaximumSumPathPattern(Node root) {
        if (root == null) {
            return "";
        }
        if (root.left == null && root.right == null) {
            return String.valueOf(root.data);
        }
        String leftSubTreeHeight = getMaximumSumPathPattern(root.left);
        String rightSubTreeHeight = getMaximumSumPathPattern(root.right);
        Integer leftVal = leftSubTreeHeight == ""?0:Integer.valueOf(leftSubTreeHeight);
        Integer rightVal = rightSubTreeHeight == ""?0:Integer.valueOf(rightSubTreeHeight);
        int i = leftVal.compareTo(rightVal);
        String greater = i>0?leftSubTreeHeight:rightSubTreeHeight;
        return greater + root.data;
    }

    private static int getPositiveNonZeroIndex(int[] array, int index) {
        return firstPositiveNonZeroIndex(array,index);
    }

    private static int firstPositiveNonZeroIndex(int[] array, int index) {
        while (index < array.length) {
            if (array[index] <= 0) {
                index++;
            } else {
                return index;
            }
        }
        // all ahead are negative or zero
        return -1;
    }

    private static boolean isArrayNegative(int[] array) {
        for (int i: array) {
            if (i > 0) {
                return false;
            }
            if (i > greatest) {
                greatest = i;
            }
        }
        return true;
    }
}

